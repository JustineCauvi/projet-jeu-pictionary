import processing.sound.*;
import javax.swing.JOptionPane;
int x1=0;   
int x2=0;
int x3=385;   //abscisses utiles au menu du haut
PImage poubelle;
int compteur=1800;  //30s
String reponse;    //mot à dessiner
String reponse2;   //mot trouvé
String[] lines;                        //tableau associé à la liste de mots
float alea=random(0,100);             //nombre aléatoire compris entre 0 et le nombre de mots de la liste
int numero=int(alea);                 //transtypage
SoundFile musique;

void setup() {
  size(560,800);
  background(255,255,255);     //fond blanc
  
  lines=loadStrings("mots.txt");                 //introduction de la liste de mots
  String reponse=lines[numero];                  //la chaîne de caractère reponse correspond au mot de la ligne choisie de façon aléatoire
  
  musique = new SoundFile(this, "SciencenetTheme.mp3");   //musique
  musique.play();
  
  for(int a=0;a<128;a+=127) {   //cases noir et gris
    fill(a,a,a);
    strokeWeight(3);           //bordures de 3pxls
    stroke(255,255,255);       //bordures blanches
    rect(x1,0,70,70);
    x1=x1+70;
  }
  
  for(int a=0;a<201;a+=200) {   //cases rose et beige
    fill(255,a,127);
    rect(x1,0,70,70);
    x1=x1+70;
  }
  
  for(int a=0;a<128;a+=127) {  //cases marron et violet
    fill(60,20,a);
    rect(x1,0,70,70);
    x1=x1+70;
  }  

  for(int a=0;a<256;a+=255) {   //cases bleu foncé et clair
    fill(0,a,255);
    rect(x1,0,70,70);
    x1=x1+70;
  }  
  
  
  for(int a=0;a<255;a+=127) {   //cases rouge, orange, jaune
    fill(255,a,0);
    rect(x2,70,70,70);
    x2=x2+70;
  }
  
  for(int a=127;a<=255;a+=128) {  //cases vert foncé et clair
    fill(0,a,0);
    rect(x2,70,70,70);
    x2=x2+70;
  }
 
  for(int r=10;r<=30;r+=10) {    //cases épaisseur
    fill(255,255,255,0);         
    stroke(0,0,0);
    strokeWeight(1);
    rect(x2,70+1.5,70,70-3);     //rectangle blanc contour noir
    x2=x2+70;
    fill(0,0,0);
    ellipse(x3,105,r,r);        //cercles de différentes tailles   
    x3=x3+70;
  }
  
  frameRate(60);    //60 fois par seconde
  
  poubelle = loadImage("poubelle.png");
  JOptionPane.showMessageDialog(null,"Bonjour et bienvenue sur notre jeu ! \n \n Ce jeu se joue à deux joueurs. Le but est de faire deviner un mot à l'autre en le dessinant. \n En début de partie, un mot sera proposé au joueur 1. Pendant ce temps là, le joueur 2 ne devra pas regarder l'écran. \n Ensuite, le joueur 1 devra dessiner ce mot dans un temps limité. Pour cela, il dispose de différentes options de dessin. \n Un menu en haut de la zone de dessin lui permet de changer la couleur ou l'épaisseur du crayon. En bas à droite, l'icône poubelle permet de retrouver une page blache. \n Vous dessinez en restant maintenu sur le clic gauche dans la surface de dessin. En appuyant sur clic droite, la fonctionnalité gomme devient active. En faisant un clic droit sur une des couleurs de la palette, la zone de dessin se remplit avec cette couleur. \n Une fois le temps écoulé, le joueur 2 doit entrer le mot qu'il pense avoir deviné avec l'orthographe correct. Le jeu vous indique alors si vous avez gagné ou perdu.  \n \n Bon jeu à vous !","Pictionary",1);
  JOptionPane.showMessageDialog(null,"Le joueur 2 ne doit pas regarder l'écran, le mot à dessiner va être affiché.", "Attention", 2);
  JOptionPane.showMessageDialog(null,"Vous devrez faire deviner le mot : "+reponse+".", "Mot à dessiner", 1);
  strokeWeight(5);  //épaisseur crayon au début  
  
  
}

void draw() {
  String reponse=lines[numero];

  if(mousePressed && mouseButton==LEFT && mouseY>160 && pmouseY>160 && mouseY<710 && pmouseY<710 && compteur>0) {     //quand le bouton gauche est appuyé dans la surface de dessin pendant le temps limité, dessine
    line(pmouseX, pmouseY, mouseX, mouseY);
  }
  
  else if(mousePressed && mouseButton==RIGHT && mouseY>160 && pmouseY>160 && mouseY<710 && pmouseY<710 && compteur>0) {    //quand le bouton droit est appuyé dans la surface de dessin pendant le temps limité, active la gomme
    stroke(255);                                                                //trait blanc
    line(pmouseX, pmouseY, mouseX, mouseY);
  }
  
  choixCouleur(0,70,0,70,0,0,0);                  //crayon noir
  choixCouleur(70,70*2,0,70,127,127,127);         //crayon gris
  choixCouleur(70*2,70*3,0,70,255,0,127);         //crayon rose
  choixCouleur(70*3,70*4,0,70,255,200,127);       //crayon beige
  choixCouleur(70*4,70*5,0,70,60,20,0);           //crayon marron
  choixCouleur(70*5,70*6,0,70,60,20,127);         //crayon violet
  choixCouleur(70*6,70*7,0,70,0,0,255);           //crayon bleu foncé
  choixCouleur(70*7,70*8,0,70,0,255,255);         //crayon bleu clair
  choixCouleur(0,70,70,140,255,0,0);              //crayon rouge
  choixCouleur(70,70*2,70,140,255,127,0);         //crayon orange
  choixCouleur(70*2,70*3,70,140,255,254,0);       //crayon jaune
  choixCouleur(70*3,70*4,70,140,0,127,0);         //crayon vert foncé
  choixCouleur(70*4,70*5,70,140,0,255,0);         //crayon vert clair
  
  remplissage(0,70,0,70,0,0,0);                  //remplissage noir
  remplissage(70,70*2,0,70,127,127,127);         //remplissage gris
  remplissage(70*2,70*3,0,70,255,0,127);         //remplissage rose
  remplissage(70*3,70*4,0,70,255,200,127);       //remplissage beige
  remplissage(70*4,70*5,0,70,60,20,0);           //remplissage marron
  remplissage(70*5,70*6,0,70,60,20,127);         //remplissage violet
  remplissage(70*6,70*7,0,70,0,0,255);           //remplissage bleu foncé
  remplissage(70*7,70*8,0,70,0,255,255);         //remplissage bleu clair
  remplissage(0,70,70,140,255,0,0);              //remplissage rouge
  remplissage(70,70*2,70,140,255,127,0);         //remplissage orange
  remplissage(70*2,70*3,70,140,255,254,0);       //remplissage jaune
  remplissage(70*3,70*4,70,140,0,127,0);         //remplissage vert foncé
  remplissage(70*4,70*5,70,140,0,255,0);         //remplissage vert clair
  
  choixEpaisseur(70*5,70*6,70,140,5);             //épaisseur crayon faible
  choixEpaisseur(70*6,70*7,70,140,20);            //épaisseur crayon moyenne
  choixEpaisseur(70*7,70*8,70,140,40);            //épaisseur crayon forte
  
  image(poubelle, 490,730,70,70);
  
    if(compteur%60==0) {       //permet d'effacer les précédentes secondes du chrono
    pushStyle();
    fill(255);
    noStroke();
    rect(0,730,75,60);
    popStyle();
    
  }
  if(compteur/60>5 && compteur%60!=0) {                 //chrono
    pushStyle();
    textSize(50);
    fill(0);
    text(str(compteur/60),10,780);
    popStyle();
  }
  if (compteur/60<=5 && compteur%60!=0) {
    pushStyle();
    textSize(50);
    fill(255,0,0);
    text(str(compteur/60),10,780);
    popStyle();
  }
  
  
  if(mousePressed && mouseButton==LEFT && mouseY>730 && mouseX>490 && compteur>0) {   //si bouton poubelle activé
    pushStyle();
    fill(255);
    noStroke();
    rect(0,145,560,725);        //zone de dessin blanc
    popStyle();
  }
  
  compteur=compteur-1;
  if(compteur==0) {
    String reponse2 = JOptionPane.showInputDialog("Joueur 2, quel mot avez-vous deviné ? Attention aux fautes d'orthographe !");
    if(reponse.equals(reponse2)==true) {
      JOptionPane.showMessageDialog(null,"Félicitations, vous avez gagné !", "Résultat", 1);
      noLoop();
    }
    else {
      JOptionPane.showMessageDialog(null,"Dommage, vous avez perdu... Le mot était "+reponse+".", "Résultat",1);
      noLoop();
    } 
  }
}

 
void choixCouleur (float xa, float xb, float ya, float yb, int a, int b, int c) {                         //fonction permettant que la couleur soit sélectionnée quand on clique sur le rectangle de couleur
  if(mousePressed && mouseButton==LEFT && mouseX>xa && mouseX<xb && mouseY>ya && mouseY<yb && compteur>0) {
    stroke(a,b,c);
  }
}
  
void choixEpaisseur (float xA, float xB, float yA, float yB, int A) {                                   //fonction pour le menu épaisseur
  if(mousePressed && mouseButton==LEFT && mouseX>xA && mouseX<xB && mouseY>yA && mouseY<yB && compteur>0) {
    strokeWeight(A);
  }
}

void remplissage (float xa, float xb, float ya, float yb, int a, int b, int c) {                           //fonction permettant que la zone de dessin soit entièrement de la couleur choisie quand on clique droit sur la couleur dans la palette
  if (mousePressed && mouseButton==RIGHT && mouseX>xa && mouseX<xb && mouseY>ya && mouseY<yb && compteur>0) {
    fill(a,b,c);
    noStroke();
    rect(0,145,560,585);
  }
}
